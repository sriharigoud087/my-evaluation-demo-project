import { Component, NgModule, OnInit } from "@angular/core";
import { audit, first } from "rxjs/operators";
import {
  Directive,
  EventEmitter,
  Input,
  Output,
  QueryList,
  ViewChildren,
} from "@angular/core";

import { Audit } from "@/_models";
import { AuditService, AuthenticationService } from "@/_services";
import { NgbPaginationModule } from "@ng-bootstrap/ng-bootstrap";
import { NgbPaginationConfig } from "@ng-bootstrap/ng-bootstrap";
import { ThrowStmt } from "@angular/compiler";

export type SortColumn = keyof Audit | "";
export type SortDirection = "asc" | "desc" | "";

const rotate: { [key: string]: SortDirection } = {
  asc: "desc",
  desc: "",
  "": "asc",
};

const compare = (v1: string | number, v2: string | number) =>
  v1 < v2 ? -1 : v1 > v2 ? 1 : 0;

export interface SortEvent {
  column: SortColumn;
  direction: SortDirection;
}

@Directive({
  selector: "th[sortable]",
  host: {
    "[class.asc]": 'direction === "asc"',
    "[class.desc]": 'direction === "desc"',
    "(click)": "rotate()",
  },
})
export class NgbdSortableHeader {
  @Input() sortable: SortColumn = "";
  @Input() direction: SortDirection = "";
  @Output() sort = new EventEmitter<SortEvent>();

  rotate() {
    this.direction = rotate[this.direction];
    this.sort.emit({ column: this.sortable, direction: this.direction });
  }
}

@Component({
  templateUrl: "audit.component.html",
  providers: [NgbPaginationConfig],
})
export class AuditComponent implements OnInit {
  audits = [];
  page = 1;
  pageSize = 4;
  formatDate = 0;
  auditsNation = [];
  collectionSize = 2000;
  hour12 = true;
  hour24 = false;
  onHourChange(event: any) {
    var val = event;
    // alert(val);
    if (val == "12") {
      this.hour12 = true;
      this.hour24 = false;
    } else {
      this.hour12 = false;
      this.hour24 = true;
    }
  }

  constructor(
    private authenticationService: AuthenticationService,
    private auditService: AuditService,
    private pagenation: NgbPaginationModule
  ) {
    //this.collectionSize = this.collectionSize + 5;
    this.refreshAudits();
  }
  FilteredPlayers: any = audit;
  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    //console.log(filterValue);
    //console.log(this.[0]);
    let filteredValue = filterValue.trim().toLowerCase();
    this.auditsNation = this.audits.filter((x) => {
      console.log(filteredValue);
      return x._id.toLowerCase().includes(filteredValue);
    });
  }

  @ViewChildren(NgbdSortableHeader) headers: QueryList<NgbdSortableHeader>;

  onSort({ column, direction }: SortEvent) {
    // resetting other headers
    this.headers.forEach((header) => {
      if (header.sortable !== column) {
        header.direction = "";
      }
    });

    // sorting audits
    if (direction === "" || column === "") {
      this.auditsNation = this.auditsNation;
    } else {
      this.auditsNation = [...this.auditsNation].sort((a, b) => {
        const res = compare(a[column], b[column]);
        // this.refreshAudits();
        return direction === "asc" ? res : -res;
      });
    }
  }

  ngOnInit() {
    this.loadAllAudits();
    this.auditsNation = this.audits;
    // alert(this.audits.length);
    //this.collectionSize = this.auditsNation.length;
    //this.collectionSize = this.audits.length;
  }
  refreshAudits() {
    // alert(this.audits.length);
    this.auditsNation = this.audits
      .map((audit, i) => ({
        id: i + 1,
        ...audit,
      }))
      .slice(
        (this.page - 1) * this.pageSize,
        (this.page - 1) * this.pageSize + this.pageSize
      );
    //alert(this.auditsNation.length);
  }

  private loadAllAudits() {
    this.auditService
      .getAll()
      .pipe(first())
      .subscribe((audits) => (this.audits = audits));
    this.auditsNation = this.audits;
  }
}
